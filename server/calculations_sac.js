function calculateSac(time_sac, depth_sac, gas, cylinder){
  let time = parseInt(time_sac);
  let depth = parseInt(depth_sac);
  let gas_used = parseInt(gas);
  let cylinder_size = parseInt(cylinder);

  let pTot = (depth/10) + 1;

  let sacRE = (gas_used*cylinder_size)/time/pTot;
  return sacRE;
}

module.exports = calculateSac;
