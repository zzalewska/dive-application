function table1Calculation(depth1, time) {
  let letter1;
  if (
    (depth1 <= 10 && time <= 10) ||
    (depth1 > 10 && depth1 <= 12 && time <= 9) ||
    (depth1 > 12 && depth1 <= 14 && time <= 8) ||
    (depth1 > 14 && depth1 <= 16 && time <= 7) ||
    (depth1 > 16 && depth1 <= 20 && time <= 6) ||
    (depth1 > 20 && depth1 <= 22 && time <= 5) ||
    (depth1 > 22 && depth1 <= 25 && time <= 4) ||
    (depth1 > 25 && depth1 <= 35 && time <= 3)
  )
    letter1 = 'A';
  else if (
    (depth1 <= 10 && time > 10 && time <= 20) ||
    (depth1 > 10 && depth1 <= 12 && time > 9 && time <= 17) ||
    (depth1 > 12 && depth1 <= 14 && time > 8 && time <= 15) ||
    (depth1 > 14 && depth1 <= 16 && time > 7 && time <= 13) ||
    (depth1 > 16 && depth1 <= 18 && time > 6 && time <= 11) ||
    (depth1 > 18 && depth1 <= 20 && time > 6 && time <= 10) ||
    (depth1 > 20 && depth1 <= 22 && time > 5 && time <= 9) ||
    (depth1 > 22 && depth1 <= 25 && time > 4 && time <= 8) ||
    (depth1 > 25 && depth1 <= 30 && time > 3 && time <= 6) ||
    (depth1 > 30 && depth1 <= 35 && time > 3 && time <= 5) ||
    (depth1 > 35 && depth1 <= 40 && time <= 5) ||
    (depth1 > 40 && depth1 <= 42 && time <= 4)
  )
    letter1 = 'B';
  else if (
    (depth1 <= 10 && time > 20 && time <= 26) ||
    (depth1 > 10 && depth1 <= 12 && time > 17 && time <= 23) ||
    (depth1 > 12 && depth1 <= 14 && time > 15 && time <= 19) ||
    (depth1 > 14 && depth1 <= 16 && time > 13 && time <= 17) ||
    (depth1 > 16 && depth1 <= 18 && time > 11 && time <= 15) ||
    (depth1 > 18 && depth1 <= 20 && time > 10 && time <= 13) ||
    (depth1 > 20 && depth1 <= 22 && time > 9 && time <= 12) ||
    (depth1 > 22 && depth1 <= 25 && time > 8 && time <= 10) ||
    (depth1 > 25 && depth1 <= 30 && time > 6 && time <= 8) ||
    (depth1 > 30 && depth1 <= 35 && time > 5 && time <= 7) ||
    (depth1 > 35 && depth1 <= 40 && time > 5 && time <= 6)
  )
    letter1 = 'C';
  else if (
    (depth1 <= 10 && time > 26 && time <= 30) ||
    (depth1 > 10 && depth1 <= 12 && time > 23 && time <= 26) ||
    (depth1 > 12 && depth1 <= 14 && time > 19 && time <= 22) ||
    (depth1 > 14 && depth1 <= 16 && time > 17 && time <= 19) ||
    (depth1 > 16 && depth1 <= 18 && time > 15 && time <= 16) ||
    (depth1 > 18 && depth1 <= 20 && time > 13 && time <= 15) ||
    (depth1 > 20 && depth1 <= 22 && time > 12 && time <= 13) ||
    (depth1 > 22 && depth1 <= 25 && time > 10 && time <= 11) ||
    (depth1 > 25 && depth1 <= 30 && time > 8 && time <= 9) ||
    (depth1 > 30 && depth1 <= 35 && time > 7 && time <= 8) ||
    (depth1 > 40 && depth1 <= 42 && time > 4 && time <= 6)
  )
    letter1 = 'D';
  else if (
    (depth1 <= 10 && time > 30 && time <= 34) ||
    (depth1 > 10 && depth1 <= 12 && time > 26 && time <= 29) ||
    (depth1 > 12 && depth1 <= 14 && time > 22 && time <= 24) ||
    (depth1 > 14 && depth1 <= 16 && time > 19 && time <= 21) ||
    (depth1 > 16 && depth1 <= 18 && time > 16 && time <= 18) ||
    (depth1 > 18 && depth1 <= 20 && time > 15 && time <= 16) ||
    (depth1 > 20 && depth1 <= 22 && time > 13 && time <= 15) ||
    (depth1 > 22 && depth1 <= 25 && time > 11 && time <= 13) ||
    (depth1 > 25 && depth1 <= 30 && time > 9 && time <= 10) ||
    (depth1 > 40 && depth1 <= 42 && time > 6 && time <= 7)
  )
    letter1 = 'E';
  else if (
    (depth1 <= 10 && time > 34 && time <= 37) ||
    (depth1 > 10 && depth1 <= 12 && time > 29 && time <= 32) ||
    (depth1 > 12 && depth1 <= 14 && time > 24 && time <= 27) ||
    (depth1 > 14 && depth1 <= 16 && time > 21 && time <= 23) ||
    (depth1 > 16 && depth1 <= 18 && time > 18 && time <= 20) ||
    (depth1 > 18 && depth1 <= 20 && time > 16 && time <= 18) ||
    (depth1 > 20 && depth1 <= 22 && time > 15 && time <= 16) ||
    (depth1 > 22 && depth1 <= 25 && time > 13 && time <= 14) ||
    (depth1 > 25 && depth1 <= 30 && time > 10 && time <= 11) ||
    (depth1 > 30 && depth1 <= 35 && time > 8 && time <= 9) ||
    (depth1 > 40 && depth1 <= 42 && time > 7 && time <= 8)
  )
    letter1 = 'F';
  else if (
    (depth1 <= 10 && time > 37 && time <= 41) ||
    (depth1 > 10 && depth1 <= 12 && time > 32 && time <= 35) ||
    (depth1 > 12 && depth1 <= 14 && time > 27 && time <= 29) ||
    (depth1 > 14 && depth1 <= 16 && time > 23 && time <= 25) ||
    (depth1 > 16 && depth1 <= 18 && time > 20 && time <= 22) ||
    (depth1 > 18 && depth1 <= 20 && time > 18 && time <= 20) ||
    (depth1 > 20 && depth1 <= 22 && time > 16 && time <= 18) ||
    (depth1 > 22 && depth1 <= 25 && time > 14 && time <= 15) ||
    (depth1 > 25 && depth1 <= 30 && time > 11 && time <= 12) ||
    (depth1 > 30 && depth1 <= 35 && time > 9 && time <= 10) ||
    (depth1 > 35 && depth1 <= 40 && time > 8 && time <= 9) ||
    (depth1 > 40 && depth1 <= 42 && time > 8 && time <= 9)
  )
    letter1 = 'G';
  else if (
    (depth1 <= 10 && time > 41 && time <= 45) ||
    (depth1 > 10 && depth1 <= 12 && time > 35 && time <= 38) ||
    (depth1 > 12 && depth1 <= 14 && time > 29 && time <= 32) ||
    (depth1 > 14 && depth1 <= 16 && time > 25 && time <= 27) ||
    (depth1 > 16 && depth1 <= 18 && time > 22 && time <= 24) ||
    (depth1 > 18 && depth1 <= 20 && time > 20 && time <= 21) ||
    (depth1 > 20 && depth1 <= 22 && time > 18 && time <= 19) ||
    (depth1 > 22 && depth1 <= 25 && time > 15 && time <= 17) ||
    (depth1 > 25 && depth1 <= 30 && time > 12 && time <= 13) ||
    (depth1 > 30 && depth1 <= 35 && time > 10 && time <= 11)
  )
    letter1 = 'H';
  else if (
    (depth1 <= 10 && time > 45 && time <= 50) ||
    (depth1 > 10 && depth1 <= 12 && time > 38 && time <= 42) ||
    (depth1 > 12 && depth1 <= 14 && time > 32 && time <= 35) ||
    (depth1 > 14 && depth1 <= 16 && time > 27 && time <= 29) ||
    (depth1 > 16 && depth1 <= 18 && time > 24 && time <= 26) ||
    (depth1 > 18 && depth1 <= 20 && time > 21 && time <= 23) ||
    (depth1 > 20 && depth1 <= 22 && time > 19 && time <= 21) ||
    (depth1 > 22 && depth1 <= 25 && time > 17 && time <= 18) ||
    (depth1 > 25 && depth1 <= 30 && time > 13 && time <= 14) ||
    (depth1 > 30 && depth1 <= 35 && time > 11 && time <= 12)
  )
    letter1 = 'I';
  else if (
    (depth1 <= 10 && time > 50 && time <= 54) ||
    (depth1 > 10 && depth1 <= 12 && time > 42 && time <= 45) ||
    (depth1 > 12 && depth1 <= 14 && time > 35 && time <= 37) ||
    (depth1 > 14 && depth1 <= 16 && time > 29 && time <= 32) ||
    (depth1 > 16 && depth1 <= 18 && time > 26 && time <= 28) ||
    (depth1 > 18 && depth1 <= 20 && time > 23 && time <= 25) ||
    (depth1 > 20 && depth1 <= 22 && time > 21 && time <= 22) ||
    (depth1 > 22 && depth1 <= 25 && time > 18 && time <= 19) ||
    (depth1 > 25 && depth1 <= 30 && time > 14 && time <= 15) ||
    (depth1 > 30 && depth1 <= 35 && time > 12 && time <= 13)
  )
    letter1 = 'J';
  else if (
    (depth1 <= 10 && time > 54 && time <= 59) ||
    (depth1 > 10 && depth1 <= 12 && time > 45 && time <= 49) ||
    (depth1 > 12 && depth1 <= 14 && time > 37 && time <= 40) ||
    (depth1 > 14 && depth1 <= 16 && time > 32 && time <= 34) ||
    (depth1 > 16 && depth1 <= 18 && time > 28 && time <= 30) ||
    (depth1 > 18 && depth1 <= 20 && time > 25 && time <= 26) ||
    (depth1 > 20 && depth1 <= 22 && time > 22 && time <= 24) ||
    (depth1 > 22 && depth1 <= 25 && time > 19 && time <= 21) ||
    (depth1 > 25 && depth1 <= 30 && time > 15 && time <= 16) ||
    (depth1 > 30 && depth1 <= 35 && time > 13 && time <= 14)
  )
    letter1 = 'K';
  else if (
    (depth1 <= 10 && time > 59 && time <= 64) ||
    (depth1 > 10 && depth1 <= 12 && time > 49 && time <= 53) ||
    (depth1 > 12 && depth1 <= 14 && time > 40 && time <= 43) ||
    (depth1 > 14 && depth1 <= 16 && time > 34 && time <= 37) ||
    (depth1 > 16 && depth1 <= 18 && time > 30 && time <= 32) ||
    (depth1 > 18 && depth1 <= 20 && time > 26 && time <= 28) ||
    (depth1 > 20 && depth1 <= 22 && time > 24 && time <= 25) ||
    (depth1 > 22 && depth1 <= 25 && time > 21 && time <= 22) ||
    (depth1 > 25 && depth1 <= 30 && time > 16 && time <= 17)
  )
    letter1 = 'L';
  else if (
    (depth1 <= 10 && time > 64 && time <= 70) ||
    (depth1 > 10 && depth1 <= 12 && time > 53 && time <= 57) ||
    (depth1 > 12 && depth1 <= 14 && time > 43 && time <= 47) ||
    (depth1 > 14 && depth1 <= 16 && time > 37 && time <= 39) ||
    (depth1 > 16 && depth1 <= 18 && time > 32 && time <= 34) ||
    (depth1 > 18 && depth1 <= 20 && time > 28 && time <= 30) ||
    (depth1 > 20 && depth1 <= 22 && time > 25 && time <= 27) ||
    (depth1 > 22 && depth1 <= 25 && time > 22 && time <= 23) ||
    (depth1 > 25 && depth1 <= 30 && time > 17 && time <= 19)
  )
    letter1 = 'M';
  else if (
    (depth1 <= 10 && time > 70 && time <= 75) ||
    (depth1 > 10 && depth1 <= 12 && time > 57 && time <= 62) ||
    (depth1 > 12 && depth1 <= 14 && time > 47 && time <= 50) ||
    (depth1 > 14 && depth1 <= 16 && time > 39 && time <= 42) ||
    (depth1 > 16 && depth1 <= 18 && time > 34 && time <= 36) ||
    (depth1 > 18 && depth1 <= 20 && time > 30 && time <= 32) ||
    (depth1 > 20 && depth1 <= 22 && time > 27 && time <= 29) ||
    (depth1 > 22 && depth1 <= 25 && time > 23 && time <= 25) ||
    (depth1 > 25 && depth1 <= 30 && time > 19 && time <= 20)
  )
    letter1 = 'N';
  else if (
    (depth1 <= 10 && time > 75 && time <= 82) ||
    (depth1 > 10 && depth1 <= 12 && time > 62 && time <= 66) ||
    (depth1 > 12 && depth1 <= 14 && time > 50 && time <= 53) ||
    (depth1 > 14 && depth1 <= 16 && time > 42 && time <= 45) ||
    (depth1 > 16 && depth1 <= 18 && time > 36 && time <= 39) ||
    (depth1 > 18 && depth1 <= 20 && time > 32 && time <= 34) ||
    (depth1 > 20 && depth1 <= 22 && time > 29 && time <= 30) ||
    (depth1 > 22 && depth1 <= 25 && time > 25 && time <= 26)
  )
    letter1 = 'O';
  else if (
    (depth1 <= 10 && time > 82 && time <= 88) ||
    (depth1 > 10 && depth1 <= 12 && time > 66 && time <= 71) ||
    (depth1 > 12 && depth1 <= 14 && time > 53 && time <= 57) ||
    (depth1 > 14 && depth1 <= 16 && time > 45 && time <= 48) ||
    (depth1 > 16 && depth1 <= 18 && time > 39 && time <= 41) ||
    (depth1 > 18 && depth1 <= 20 && time > 34 && time <= 36) ||
    (depth1 > 20 && depth1 <= 22 && time > 30 && time <= 32) ||
    (depth1 > 22 && depth1 <= 25 && time > 26 && time <= 28)
  )
    letter1 = 'P';
  else if (
    (depth1 <= 10 && time > 88 && time <= 95) ||
    (depth1 > 10 && depth1 <= 12 && time > 71 && time <= 76) ||
    (depth1 > 12 && depth1 <= 14 && time > 57 && time <= 61) ||
    (depth1 > 14 && depth1 <= 16 && time > 48 && time <= 50) ||
    (depth1 > 16 && depth1 <= 18 && time > 41 && time <= 43) ||
    (depth1 > 18 && depth1 <= 20 && time > 36 && time <= 38) ||
    (depth1 > 20 && depth1 <= 22 && time > 32 && time <= 34) ||
    (depth1 > 22 && depth1 <= 25 && time > 28 && time <= 29)
  )
    letter1 = 'Q';
  else if (
    (depth1 <= 10 && time > 95 && time <= 104) ||
    (depth1 > 10 && depth1 <= 12 && time > 76 && time <= 83) ||
    (depth1 > 12 && depth1 <= 14 && time > 61 && time <= 64) ||
    (depth1 > 14 && depth1 <= 16 && time > 50 && time <= 53) ||
    (depth1 > 16 && depth1 <= 18 && time > 43 && time <= 46) ||
    (depth1 > 18 && depth1 <= 20 && time > 38 && time <= 40) ||
    (depth1 > 20 && depth1 <= 22 && time > 34 && time <= 36)
  )
    letter1 = 'R';
  else if (
    (depth1 <= 10 && time > 104 && time <= 112) ||
    (depth1 > 10 && depth1 <= 12 && time > 82 && time <= 88) ||
    (depth1 > 12 && depth1 <= 14 && time > 64 && time <= 68) ||
    (depth1 > 14 && depth1 <= 16 && time > 53 && time <= 56) ||
    (depth1 > 16 && depth1 <= 18 && time > 46 && time <= 48) ||
    (depth1 > 18 && depth1 <= 20 && time > 40 && time <= 42) ||
    (depth1 > 20 && depth1 <= 22 && time > 36 && time <= 37)
  )
    letter1 = 'S';
  else if (
    (depth1 <= 10 && time > 112 && time <= 122) ||
    (depth1 > 10 && depth1 <= 12 && time > 88 && time <= 94) ||
    (depth1 > 12 && depth1 <= 14 && time > 68 && time <= 73) ||
    (depth1 > 14 && depth1 <= 16 && time > 56 && time <= 60) ||
    (depth1 > 16 && depth1 <= 18 && time > 48 && time <= 51) ||
    (depth1 > 18 && depth1 <= 20 && time > 42 && time <= 44)
  )
    letter1 = 'T';
  else if (
    (depth1 <= 10 && time > 122 && time <= 133) ||
    (depth1 > 10 && depth1 <= 12 && time > 94 && time <= 101) ||
    (depth1 > 12 && depth1 <= 14 && time > 73 && time <= 77) ||
    (depth1 > 14 && depth1 <= 16 && time > 60 && time <= 63) ||
    (depth1 > 16 && depth1 <= 18 && time > 51 && time <= 53) ||
    (depth1 > 18 && depth1 <= 20 && time > 44 && time <= 45)
  )
    letter1 = 'U';
  else if (
    (depth1 <= 10 && time > 133 && time <= 145) ||
    (depth1 > 10 && depth1 <= 12 && time > 101 && time <= 108) ||
    (depth1 > 12 && depth1 <= 14 && time > 77 && time <= 82) ||
    (depth1 > 14 && depth1 <= 16 && time > 63 && time <= 67) ||
    (depth1 > 16 && depth1 <= 18 && time > 53 && time <= 55)
  )
    letter1 = 'V';
  else if (
    (depth1 <= 10 && time > 145 && time <= 160) ||
    (depth1 > 10 && depth1 <= 12 && time > 108 && time <= 116) ||
    (depth1 > 12 && depth1 <= 14 && time > 82 && time <= 87) ||
    (depth1 > 14 && depth1 <= 16 && time > 67 && time <= 70) ||
    (depth1 > 16 && depth1 <= 18 && time > 55 && time <= 56)
  )
    letter1 = 'W';
  else if (
    (depth1 <= 10 && time > 160 && time <= 178) ||
    (depth1 > 10 && depth1 <= 12 && time > 116 && time <= 125) ||
    (depth1 > 12 && depth1 <= 14 && time > 87 && time <= 92) ||
    (depth1 > 14 && depth1 <= 16 && time > 70 && time <= 72)
  )
    letter1 = 'X';
  else if (
    (depth1 <= 10 && time > 178 && time <= 199) ||
    (depth1 > 10 && depth1 <= 12 && time > 125 && time <= 134) ||
    (depth1 > 12 && depth1 <= 14 && time > 92 && time <= 98)
  )
    letter1 = 'Y';
  else if ((depth1 <= 10 && time > 100 && time <= 219) || (depth1 > 10 && depth1 <= 12 && time > 134 && time <= 147))
    letter1 = 'Z';
  else letter1 = '';

  return letter1;
}

function calculate_rnt_and_abt(letter2, depth2) {
  let rnt;
  let abt;
  switch (letter2) {
    case 'A':
      if (depth2 <= 10) {
        rnt = 10;
        abt = 209;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 9;
        abt = 138;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 8;
        abt = 90;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 7;
        abt = 65;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 6;
        abt = 50;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 6;
        abt = 39;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 5;
        abt = 32;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 4;
        abt = 25;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 3;
        abt = 17;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 3;
        abt = 11;
      } else if (depth2 > 35 && depth2 <= 40) {
        rnt = 2;
        abt = 7;
      }
      break;

    case 'B':
      if (depth2 <= 10) {
        rnt = 20;
        abt = 209;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 17;
        abt = 130;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 15;
        abt = 83;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 13;
        abt = 59;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 11;
        abt = 45;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 10;
        abt = 35;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 9;
        abt = 28;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 8;
        abt = 21;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 6;
        abt = 14;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 5;
        abt = 9;
      } else if (depth2 > 35 && depth2 <= 40) {
        rnt = 5;
        abt = 4;
      }
      break;

    case 'C':
      if (depth2 <= 10) {
        rnt = 26;
        abt = 193;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 23;
        abt = 124;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 19;
        abt = 79;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 17;
        abt = 55;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 15;
        abt = 41;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 13;
        abt = 32;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 12;
        abt = 25;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 10;
        abt = 19;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 8;
        abt = 12;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 7;
        abt = 7;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 7;
      }
      break;

    case 'D':
      if (depth2 <= 10) {
        rnt = 30;
        abt = 189;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 26;
        abt = 121;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 22;
        abt = 76;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 19;
        abt = 53;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 16;
        abt = 40;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 15;
        abt = 30;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 13;
        abt = 24;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 11;
        abt = 18;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 9;
        abt = 11;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 8;
        abt = 6;
      } else if (depth2 > 35 && depth2 <= 40) {
        rnt = 7;
      }
      break;
    case 'E':
      if (depth2 <= 10) {
        rnt = 34;
        abt = 185;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 29;
        abt = 118;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 24;
        abt = 74;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 21;
        abt = 51;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 18;
        abt = 38;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 16;
        abt = 29;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 15;
        abt = 22;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 13;
        abt = 16;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 10;
        abt = 10;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 9;
        abt = 5;
      } else if (depth2 > 35 && depth2 <= 40) {
        rnt = 7;
      }
      break;
    case 'F':
      if (depth2 <= 10) {
        rnt = 37;
        abt = 182;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 32;
        abt = 115;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 27;
        abt = 71;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 23;
        abt = 49;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 20;
        abt = 36;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 18;
        abt = 27;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 16;
        abt = 21;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 14;
        abt = 15;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 11;
        abt = 9;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 9;
        abt = 5;
      } else if (depth2 > 35 && depth2 <= 40) {
        rnt = 8;
      }
      break;
    case 'G':
      if (depth2 <= 10) {
        rnt = 41;
        abt = 178;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 35;
        abt = 112;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 29;
        abt = 89;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 25;
        abt = 47;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 22;
        abt = 34;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 20;
        abt = 25;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 18;
        abt = 19;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 15;
        abt = 14;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 12;
        abt = 8;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 10;
        abt = 4;
      } else if (depth2 > 35 && depth2 <= 40) {
        rnt = 9;
      }
      break;
    case 'H':
      if (depth2 <= 10) {
        rnt = 45;
        abt = 174;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 38;
        abt = 109;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 32;
        abt = 68;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 27;
        abt = 45;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 24;
        abt = 32;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 21;
        abt = 24;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 19;
        abt = 18;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 17;
        abt = 12;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 13;
        abt = 7;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 11;
        abt = 3;
      }
      break;
    case 'I':
      if (depth2 <= 10) {
        rnt = 50;
        abt = 169;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 42;
        abt = 105;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 35;
        abt = 63;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 29;
        abt = 43;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 26;
        abt = 30;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 23;
        abt = 22;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 21;
        abt = 16;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 18;
        abt = 11;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 14;
        abt = 6;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 12;
      }
      break;
    case 'J':
      if (depth2 <= 10) {
        rnt = 54;
        abt = 165;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 45;
        abt = 102;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 37;
        abt = 61;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 32;
        abt = 40;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 28;
        abt = 28;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 25;
        abt = 20;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 22;
        abt = 15;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 19;
        abt = 10;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 15;
        abt = 5;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 13;
      }
      break;
    case 'K':
      if (depth2 <= 10) {
        rnt = 59;
        abt = 160;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 49;
        abt = 98;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 40;
        abt = 58;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 34;
        abt = 38;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 30;
        abt = 26;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 26;
        abt = 19;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 24;
        abt = 13;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 21;
        abt = 8;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 16;
        abt = 4;
      } else if (depth2 > 30 && depth2 <= 35) {
        rnt = 14;
      }
      break;
    case 'L':
      if (depth2 <= 10) {
        rnt = 64;
        abt = 155;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 53;
        abt = 94;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 43;
        abt = 55;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 37;
        abt = 35;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 32;
        abt = 24;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 28;
        abt = 17;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 25;
        abt = 12;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 22;
        abt = 7;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 17;
        abt = 3;
      }
      break;
    case 'M':
      if (depth2 <= 10) {
        rnt = 70;
        abt = 149;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 57;
        abt = 90;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 47;
        abt = 51;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 39;
        abt = 33;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 34;
        abt = 22;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 30;
        abt = 15;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 27;
        abt = 10;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 23;
        abt = 6;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 19;
      }
      break;
    case 'N':
      if (depth2 <= 10) {
        rnt = 75;
        abt = 144;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 62;
        abt = 85;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 50;
        abt = 48;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 42;
        abt = 30;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 36;
        abt = 20;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 32;
        abt = 13;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 29;
        abt = 8;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 25;
        abt = 4;
      } else if (depth2 > 25 && depth2 <= 30) {
        rnt = 20;
      }
      break;
    case 'O':
      if (depth2 <= 10) {
        rnt = 82;
        abt = 137;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 66;
        abt = 81;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 53;
        abt = 45;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 45;
        abt = 27;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 39;
        abt = 17;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 34;
        abt = 11;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 30;
        abt = 7;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 26;
        abt = 3;
      }
      break;
    case 'P':
      if (depth2 <= 10) {
        rnt = 88;
        abt = 131;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 71;
        abt = 76;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 57;
        abt = 41;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 48;
        abt = 24;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 41;
        abt = 15;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 36;
        abt = 9;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 32;
        abt = 5;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 28;
      }
      break;
    case 'Q':
      if (depth2 <= 10) {
        rnt = 95;
        abt = 124;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 76;
        abt = 71;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 61;
        abt = 37;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 50;
        abt = 22;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 43;
        abt = 13;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 38;
        abt = 7;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 34;
        abt = 3;
      } else if (depth2 > 22 && depth2 <= 25) {
        rnt = 29;
      }
      break;
    case 'R':
      if (depth2 <= 10) {
        rnt = 104;
        abt = 115;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 82;
        abt = 65;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 64;
        abt = 34;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 53;
        abt = 19;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 46;
        abt = 10;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 40;
        abt = 5;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 36;
      }
      break;
    case 'S':
      if (depth2 <= 10) {
        rnt = 112;
        abt = 107;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 88;
        abt = 59;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 68;
        abt = 30;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 56;
        abt = 16;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 48;
        abt = 8;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 42;
        abt = 3;
      } else if (depth2 > 20 && depth2 <= 22) {
        rnt = 37;
      }
      break;
    case 'T':
      if (depth2 <= 10) {
        rnt = 122;
        abt = 97;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 94;
        abt = 53;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 73;
        abt = 25;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 60;
        abt = 12;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 51;
        abt = 5;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 44;
      }
      break;
    case 'U':
      if (depth2 <= 10) {
        rnt = 133;
        abt = 86;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 101;
        abt = 46;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 77;
        abt = 21;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 63;
        abt = 9;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 53;
        abt = 3;
      } else if (depth2 > 18 && depth2 <= 20) {
        rnt = 45;
      }
      break;
    case 'V':
      if (depth2 <= 10) {
        rnt = 145;
        abt = 74;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 108;
        abt = 39;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 82;
        abt = 16;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 67;
        abt = 5;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 55;
      }
      break;
    case 'W':
      if (depth2 <= 10) {
        rnt = 160;
        abt = 59;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 116;
        abt = 31;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 87;
        abt = 11;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 70;
        abt = 2;
      } else if (depth2 > 16 && depth2 <= 18) {
        rnt = 56;
      }
      break;
    case 'X':
      if (depth2 <= 10) {
        rnt = 178;
        abt = 41;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 125;
        abt = 22;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 92;
        abt = 6;
      } else if (depth2 > 14 && depth2 <= 16) {
        rnt = 72;
      }
      break;
    case 'Y':
      if (depth2 <= 10) {
        rnt = 199;
        abt = 20;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 134;
        abt = 13;
      } else if (depth2 > 12 && depth2 <= 14) {
        rnt = 98;
      }
      break;
    case 'Z':
      if (depth2 <= 10) {
        rnt = 219;
      } else if (depth2 > 10 && depth2 <= 12) {
        rnt = 147;
      }
      break;
  }
  return { rnt, abt };
}

function table2Calculation(surface_time, letter1) {
  let letter2;
  //Table 2 - group after first dive and sirface interval
  if (letter1 == 'A') letter2 = surface_time >= 0 ? 'A' : letter2;
  else if (letter1 == 'B') {
    letter2 = surface_time >= 48 ? 'A' : letter2;
    letter2 = surface_time <= 47 ? 'B' : letter2;
  } else if (letter1 == 'C') {
    letter2 = surface_time >= 70 ? 'A' : letter2;
    letter2 = surface_time >= 22 && surface_time <= 69 ? 'B' : letter2;
    letter2 = surface_time <= 21 ? 'C' : letter2;
  } else if (letter1 == 'D') {
    letter2 = surface_time >= 79 ? 'A' : letter2;
    letter2 = surface_time >= 31 && surface_time <= 78 ? 'B' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 30 ? 'C' : letter2;
    letter2 = surface_time <= 8 ? 'D' : letter2;
  } else if (letter1 == 'E') {
    letter2 = surface_time >= 88 ? 'A' : letter2;
    letter2 = surface_time >= 39 && surface_time <= 87 ? 'B' : letter2;
    letter2 = surface_time >= 17 && surface_time <= 38 ? 'C' : letter2;
    letter2 = surface_time >= 8 && surface_time <= 16 ? 'D' : letter2;
    letter2 = surface_time <= 7 ? 'E' : letter2;
  } else if (letter1 == 'F') {
    letter2 = surface_time >= 95 ? 'A' : letter2;
    letter2 = surface_time >= 47 && surface_time <= 94 ? 'B' : letter2;
    letter2 = surface_time >= 25 && surface_time <= 46 ? 'C' : letter2;
    letter2 = surface_time >= 16 && surface_time <= 24 ? 'D' : letter2;
    letter2 = surface_time >= 8 && surface_time <= 15 ? 'E' : letter2;
    letter2 = surface_time <= 7 ? 'F' : letter2;
  } else if (letter1 == 'G') {
    letter2 = surface_time >= 102 ? 'A' : letter2;
    letter2 = surface_time >= 54 && surface_time <= 101 ? 'B' : letter2;
    letter2 = surface_time >= 32 && surface_time <= 53 ? 'C' : letter2;
    letter2 = surface_time >= 23 && surface_time <= 31 ? 'D' : letter2;
    letter2 = surface_time >= 14 && surface_time <= 22 ? 'E' : letter2;
    letter2 = surface_time >= 7 && surface_time <= 13 ? 'F' : letter2;
    letter2 = surface_time <= 6 ? 'G' : letter2;
  } else if (letter1 == 'H') {
    letter2 = surface_time >= 108 ? 'A' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 107 ? 'B' : letter2;
    letter2 = surface_time >= 38 && surface_time <= 59 ? 'C' : letter2;
    letter2 = surface_time >= 29 && surface_time <= 37 ? 'D' : letter2;
    letter2 = surface_time >= 21 && surface_time <= 28 ? 'E' : letter2;
    letter2 = surface_time >= 13 && surface_time <= 20 ? 'F' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 12 ? 'G' : letter2;
    letter2 = surface_time <= 5 ? 'H' : letter2;
  } else if (letter1 == 'I') {
    letter2 = surface_time >= 114 ? 'A' : letter2;
    letter2 = surface_time >= 66 && surface_time <= 113 ? 'B' : letter2;
    letter2 = surface_time >= 44 && surface_time <= 65 ? 'C' : letter2;
    letter2 = surface_time >= 35 && surface_time <= 43 ? 'D' : letter2;
    letter2 = surface_time >= 27 && surface_time <= 34 ? 'E' : letter2;
    letter2 = surface_time >= 19 && surface_time <= 26 ? 'F' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 18 ? 'G' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 11 ? 'H' : letter2;
    letter2 = surface_time <= 5 ? 'I' : letter2;
  } else if (letter1 == 'J') {
    letter2 = surface_time >= 120 ? 'A' : letter2;
    letter2 = surface_time >= 72 && surface_time <= 119 ? 'B' : letter2;
    letter2 = surface_time >= 50 && surface_time <= 71 ? 'C' : letter2;
    letter2 = surface_time >= 41 && surface_time <= 49 ? 'D' : letter2;
    letter2 = surface_time >= 32 && surface_time <= 40 ? 'E' : letter2;
    letter2 = surface_time >= 25 && surface_time <= 31 ? 'F' : letter2;
    letter2 = surface_time >= 18 && surface_time <= 24 ? 'G' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 17 ? 'H' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 11 ? 'I' : letter2;
    letter2 = surface_time <= 5 ? 'J' : letter2;
  } else if (letter1 == 'K') {
    letter2 = surface_time >= 125 ? 'A' : letter2;
    letter2 = surface_time >= 77 && surface_time <= 124 ? 'B' : letter2;
    letter2 = surface_time >= 55 && surface_time <= 76 ? 'C' : letter2;
    letter2 = surface_time >= 46 && surface_time <= 54 ? 'D' : letter2;
    letter2 = surface_time >= 38 && surface_time <= 45 ? 'E' : letter2;
    letter2 = surface_time >= 30 && surface_time <= 37 ? 'F' : letter2;
    letter2 = surface_time >= 23 && surface_time <= 29 ? 'G' : letter2;
    letter2 = surface_time >= 17 && surface_time <= 22 ? 'H' : letter2;
    letter2 = surface_time >= 11 && surface_time <= 16 ? 'I' : letter2;
    letter2 = surface_time >= 5 && surface_time <= 10 ? 'J' : letter2;
    letter2 = surface_time <= 4 ? 'K' : letter2;
  } else if (letter1 == 'L') {
    letter2 = surface_time >= 130 ? 'A' : letter2;
    letter2 = surface_time >= 82 && surface_time <= 129 ? 'B' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 81 ? 'C' : letter2;
    letter2 = surface_time >= 51 && surface_time <= 59 ? 'D' : letter2;
    letter2 = surface_time >= 43 && surface_time <= 50 ? 'E' : letter2;
    letter2 = surface_time >= 35 && surface_time <= 42 ? 'F' : letter2;
    letter2 = surface_time >= 28 && surface_time <= 34 ? 'G' : letter2;
    letter2 = surface_time >= 22 && surface_time <= 27 ? 'H' : letter2;
    letter2 = surface_time >= 16 && surface_time <= 21 ? 'I' : letter2;
    letter2 = surface_time >= 10 && surface_time <= 15 ? 'J' : letter2;
    letter2 = surface_time >= 5 && surface_time <= 9 ? 'K' : letter2;
    letter2 = surface_time <= 4 ? 'L' : letter2;
  } else if (letter1 == 'M') {
    letter2 = surface_time >= 135 ? 'A' : letter2;
    letter2 = surface_time >= 86 && surface_time <= 134 ? 'B' : letter2;
    letter2 = surface_time >= 65 && surface_time <= 85 ? 'C' : letter2;
    letter2 = surface_time >= 56 && surface_time <= 64 ? 'D' : letter2;
    letter2 = surface_time >= 47 && surface_time <= 55 ? 'E' : letter2;
    letter2 = surface_time >= 40 && surface_time <= 46 ? 'F' : letter2;
    letter2 = surface_time >= 33 && surface_time <= 39 ? 'G' : letter2;
    letter2 = surface_time >= 26 && surface_time <= 32 ? 'H' : letter2;
    letter2 = surface_time >= 20 && surface_time <= 25 ? 'I' : letter2;
    letter2 = surface_time >= 15 && surface_time <= 19 ? 'J' : letter2;
    letter2 = surface_time >= 10 && surface_time <= 14 ? 'K' : letter2;
    letter2 = surface_time >= 5 && surface_time <= 9 ? 'L' : letter2;
    letter2 = surface_time <= 4 ? 'M' : letter2;
  } else if (letter1 == 'N') {
    letter2 = surface_time >= 139 ? 'A' : letter2;
    letter2 = surface_time >= 91 && surface_time <= 138 ? 'B' : letter2;
    letter2 = surface_time >= 69 && surface_time <= 90 ? 'C' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 68 ? 'D' : letter2;
    letter2 = surface_time >= 52 && surface_time <= 59 ? 'E' : letter2;
    letter2 = surface_time >= 44 && surface_time <= 51 ? 'F' : letter2;
    letter2 = surface_time >= 37 && surface_time <= 43 ? 'G' : letter2;
    letter2 = surface_time >= 31 && surface_time <= 36 ? 'H' : letter2;
    letter2 = surface_time >= 25 && surface_time <= 30 ? 'I' : letter2;
    letter2 = surface_time >= 19 && surface_time <= 24 ? 'J' : letter2;
    letter2 = surface_time >= 14 && surface_time <= 18 ? 'K' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 13 ? 'L' : letter2;
    letter2 = surface_time >= 4 && surface_time <= 8 ? 'M' : letter2;
    letter2 = surface_time <= 3 ? 'N' : letter2;
  } else if (letter1 == 'O') {
    letter2 = surface_time >= 144 ? 'A' : letter2;
    letter2 = surface_time >= 95 && surface_time <= 143 ? 'B' : letter2;
    letter2 = surface_time >= 73 && surface_time <= 94 ? 'C' : letter2;
    letter2 = surface_time >= 64 && surface_time <= 72 ? 'D' : letter2;
    letter2 = surface_time >= 56 && surface_time <= 63 ? 'E' : letter2;
    letter2 = surface_time >= 48 && surface_time <= 55 ? 'F' : letter2;
    letter2 = surface_time >= 42 && surface_time <= 47 ? 'G' : letter2;
    letter2 = surface_time >= 35 && surface_time <= 41 ? 'H' : letter2;
    letter2 = surface_time >= 29 && surface_time <= 34 ? 'I' : letter2;
    letter2 = surface_time >= 24 && surface_time <= 28 ? 'J' : letter2;
    letter2 = surface_time >= 18 && surface_time <= 23 ? 'K' : letter2;
    letter2 = surface_time >= 13 && surface_time <= 17 ? 'L' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 12 ? 'M' : letter2;
    letter2 = surface_time >= 4 && surface_time <= 8 ? 'N' : letter2;
    letter2 = surface_time <= 3 ? 'O' : letter2;
  } else if (letter1 == 'P') {
    letter2 = surface_time >= 148 ? 'A' : letter2;
    letter2 = surface_time >= 99 && surface_time <= 147 ? 'B' : letter2;
    letter2 = surface_time >= 77 && surface_time <= 98 ? 'C' : letter2;
    letter2 = surface_time >= 68 && surface_time <= 76 ? 'D' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 67 ? 'E' : letter2;
    letter2 = surface_time >= 52 && surface_time <= 59 ? 'F' : letter2;
    letter2 = surface_time >= 46 && surface_time <= 51 ? 'G' : letter2;
    letter2 = surface_time >= 39 && surface_time <= 45 ? 'H' : letter2;
    letter2 = surface_time >= 33 && surface_time <= 38 ? 'I' : letter2;
    letter2 = surface_time >= 28 && surface_time <= 32 ? 'J' : letter2;
    letter2 = surface_time >= 22 && surface_time <= 27 ? 'K' : letter2;
    letter2 = surface_time >= 17 && surface_time <= 21 ? 'L' : letter2;
    letter2 = surface_time >= 13 && surface_time <= 16 ? 'M' : letter2;
    letter2 = surface_time >= 8 && surface_time <= 12 ? 'N' : letter2;
    letter2 = surface_time >= 4 && surface_time <= 7 ? 'O' : letter2;
    letter2 = surface_time <= 3 ? 'P' : letter2;
  } else if (letter1 == 'Q') {
    letter2 = surface_time >= 151 ? 'A' : letter2;
    letter2 = surface_time >= 103 && surface_time <= 150 ? 'B' : letter2;
    letter2 = surface_time >= 81 && surface_time <= 102 ? 'C' : letter2;
    letter2 = surface_time >= 72 && surface_time <= 80 ? 'D' : letter2;
    letter2 = surface_time >= 64 && surface_time <= 71 ? 'E' : letter2;
    letter2 = surface_time >= 56 && surface_time <= 63 ? 'F' : letter2;
    letter2 = surface_time >= 49 && surface_time <= 55 ? 'G' : letter2;
    letter2 = surface_time >= 43 && surface_time <= 48 ? 'H' : letter2;
    letter2 = surface_time >= 37 && surface_time <= 42 ? 'I' : letter2;
    letter2 = surface_time >= 31 && surface_time <= 36 ? 'J' : letter2;
    letter2 = surface_time >= 26 && surface_time <= 30 ? 'K' : letter2;
    letter2 = surface_time >= 21 && surface_time <= 25 ? 'L' : letter2;
    letter2 = surface_time >= 17 && surface_time <= 20 ? 'M' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 16 ? 'N' : letter2;
    letter2 = surface_time >= 8 && surface_time <= 11 ? 'O' : letter2;
    letter2 = surface_time >= 4 && surface_time <= 7 ? 'P' : letter2;
    letter2 = surface_time <= 3 ? 'Q' : letter2;
  } else if (letter1 == 'R') {
    letter2 = surface_time >= 155 ? 'A' : letter2;
    letter2 = surface_time >= 107 && surface_time <= 154 ? 'B' : letter2;
    letter2 = surface_time >= 85 && surface_time <= 106 ? 'C' : letter2;
    letter2 = surface_time >= 76 && surface_time <= 84 ? 'D' : letter2;
    letter2 = surface_time >= 68 && surface_time <= 75 ? 'E' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 67 ? 'F' : letter2;
    letter2 = surface_time >= 53 && surface_time <= 59 ? 'G' : letter2;
    letter2 = surface_time >= 47 && surface_time <= 52 ? 'H' : letter2;
    letter2 = surface_time >= 41 && surface_time <= 46 ? 'I' : letter2;
    letter2 = surface_time >= 35 && surface_time <= 40 ? 'J' : letter2;
    letter2 = surface_time >= 30 && surface_time <= 34 ? 'K' : letter2;
    letter2 = surface_time >= 25 && surface_time <= 29 ? 'L' : letter2;
    letter2 = surface_time >= 20 && surface_time <= 24 ? 'M' : letter2;
    letter2 = surface_time >= 16 && surface_time <= 19 ? 'N' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 15 ? 'O' : letter2;
    letter2 = surface_time >= 8 && surface_time <= 11 ? 'P' : letter2;
    letter2 = surface_time >= 4 && surface_time <= 7 ? 'Q' : letter2;
    letter2 = surface_time <= 3 ? 'R' : letter2;
  } else if (letter1 == 'S') {
    letter2 = surface_time >= 159 ? 'A' : letter2;
    letter2 = surface_time >= 110 && surface_time <= 158 ? 'B' : letter2;
    letter2 = surface_time >= 88 && surface_time <= 109 ? 'C' : letter2;
    letter2 = surface_time >= 79 && surface_time <= 87 ? 'D' : letter2;
    letter2 = surface_time >= 71 && surface_time <= 78 ? 'E' : letter2;
    letter2 = surface_time >= 64 && surface_time <= 70 ? 'F' : letter2;
    letter2 = surface_time >= 57 && surface_time <= 63 ? 'G' : letter2;
    letter2 = surface_time >= 50 && surface_time <= 56 ? 'H' : letter2;
    letter2 = surface_time >= 44 && surface_time <= 49 ? 'I' : letter2;
    letter2 = surface_time >= 39 && surface_time <= 43 ? 'J' : letter2;
    letter2 = surface_time >= 33 && surface_time <= 38 ? 'K' : letter2;
    letter2 = surface_time >= 28 && surface_time <= 32 ? 'L' : letter2;
    letter2 = surface_time >= 24 && surface_time <= 27 ? 'M' : letter2;
    letter2 = surface_time >= 19 && surface_time <= 23 ? 'N' : letter2;
    letter2 = surface_time >= 15 && surface_time <= 18 ? 'O' : letter2;
    letter2 = surface_time >= 11 && surface_time <= 14 ? 'P' : letter2;
    letter2 = surface_time >= 7 && surface_time <= 10 ? 'Q' : letter2;
    letter2 = surface_time >= 4 && surface_time <= 6 ? 'R' : letter2;
    letter2 = surface_time <= 3 ? 'S' : letter2;
  } else if (letter1 == 'T') {
    letter2 = surface_time >= 162 ? 'A' : letter2;
    letter2 = surface_time >= 114 && surface_time <= 161 ? 'B' : letter2;
    letter2 = surface_time >= 92 && surface_time <= 113 ? 'C' : letter2;
    letter2 = surface_time >= 83 && surface_time <= 91 ? 'D' : letter2;
    letter2 = surface_time >= 74 && surface_time <= 82 ? 'E' : letter2;
    letter2 = surface_time >= 67 && surface_time <= 73 ? 'F' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 66 ? 'G' : letter2;
    letter2 = surface_time >= 54 && surface_time <= 59 ? 'H' : letter2;
    letter2 = surface_time >= 48 && surface_time <= 53 ? 'I' : letter2;
    letter2 = surface_time >= 42 && surface_time <= 47 ? 'J' : letter2;
    letter2 = surface_time >= 37 && surface_time <= 41 ? 'K' : letter2;
    letter2 = surface_time >= 32 && surface_time <= 36 ? 'L' : letter2;
    letter2 = surface_time >= 27 && surface_time <= 31 ? 'M' : letter2;
    letter2 = surface_time >= 23 && surface_time <= 26 ? 'N' : letter2;
    letter2 = surface_time >= 18 && surface_time <= 22 ? 'O' : letter2;
    letter2 = surface_time >= 14 && surface_time <= 17 ? 'P' : letter2;
    letter2 = surface_time >= 11 && surface_time <= 13 ? 'Q' : letter2;
    letter2 = surface_time >= 7 && surface_time <= 10 ? 'R' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 6 ? 'S' : letter2;
    letter2 = surface_time <= 2 ? 'T' : letter2;
  } else if (letter1 == 'U') {
    letter2 = surface_time >= 165 ? 'A' : letter2;
    letter2 = surface_time >= 117 && surface_time <= 165 ? 'B' : letter2;
    letter2 = surface_time >= 95 && surface_time <= 116 ? 'C' : letter2;
    letter2 = surface_time >= 86 && surface_time <= 94 ? 'D' : letter2;
    letter2 = surface_time >= 78 && surface_time <= 85 ? 'E' : letter2;
    letter2 = surface_time >= 70 && surface_time <= 77 ? 'F' : letter2;
    letter2 = surface_time >= 63 && surface_time <= 69 ? 'G' : letter2;
    letter2 = surface_time >= 57 && surface_time <= 62 ? 'H' : letter2;
    letter2 = surface_time >= 51 && surface_time <= 56 ? 'I' : letter2;
    letter2 = surface_time >= 45 && surface_time <= 50 ? 'J' : letter2;
    letter2 = surface_time >= 40 && surface_time <= 44 ? 'K' : letter2;
    letter2 = surface_time >= 35 && surface_time <= 39 ? 'L' : letter2;
    letter2 = surface_time >= 30 && surface_time <= 34 ? 'M' : letter2;
    letter2 = surface_time >= 26 && surface_time <= 29 ? 'N' : letter2;
    letter2 = surface_time >= 22 && surface_time <= 25 ? 'O' : letter2;
    letter2 = surface_time >= 18 && surface_time <= 21 ? 'P' : letter2;
    letter2 = surface_time >= 14 && surface_time <= 17 ? 'Q' : letter2;
    letter2 = surface_time >= 10 && surface_time <= 13 ? 'R' : letter2;
    letter2 = surface_time >= 7 && surface_time <= 9 ? 'S' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 6 ? 'T' : letter2;
    letter2 = surface_time <= 2 ? 'U' : letter2;
  } else if (letter1 == 'V') {
    letter2 = surface_time >= 168 ? 'A' : letter2;
    letter2 = surface_time >= 120 && surface_time <= 167 ? 'B' : letter2;
    letter2 = surface_time >= 98 && surface_time <= 119 ? 'C' : letter2;
    letter2 = surface_time >= 89 && surface_time <= 97 ? 'D' : letter2;
    letter2 = surface_time >= 81 && surface_time <= 88 ? 'E' : letter2;
    letter2 = surface_time >= 73 && surface_time <= 80 ? 'F' : letter2;
    letter2 = surface_time >= 66 && surface_time <= 72 ? 'G' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 65 ? 'H' : letter2;
    letter2 = surface_time >= 54 && surface_time <= 59 ? 'I' : letter2;
    letter2 = surface_time >= 48 && surface_time <= 53 ? 'J' : letter2;
    letter2 = surface_time >= 43 && surface_time <= 47 ? 'K' : letter2;
    letter2 = surface_time >= 38 && surface_time <= 42 ? 'L' : letter2;
    letter2 = surface_time >= 34 && surface_time <= 37 ? 'M' : letter2;
    letter2 = surface_time >= 29 && surface_time <= 33 ? 'N' : letter2;
    letter2 = surface_time >= 25 && surface_time <= 28 ? 'O' : letter2;
    letter2 = surface_time >= 21 && surface_time <= 24 ? 'P' : letter2;
    letter2 = surface_time >= 17 && surface_time <= 20 ? 'Q' : letter2;
    letter2 = surface_time >= 13 && surface_time <= 16 ? 'R' : letter2;
    letter2 = surface_time >= 10 && surface_time <= 12 ? 'S' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 9 ? 'T' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 5 ? 'U' : letter2;
    letter2 = surface_time <= 2 ? 'V' : letter2;
  } else if (letter1 == 'W') {
    letter2 = surface_time >= 171 ? 'A' : letter2;
    letter2 = surface_time >= 123 && surface_time <= 170 ? 'B' : letter2;
    letter2 = surface_time >= 101 && surface_time <= 122 ? 'C' : letter2;
    letter2 = surface_time >= 92 && surface_time <= 100 ? 'D' : letter2;
    letter2 = surface_time >= 84 && surface_time <= 91 ? 'E' : letter2;
    letter2 = surface_time >= 76 && surface_time <= 83 ? 'F' : letter2;
    letter2 = surface_time >= 69 && surface_time <= 75 ? 'G' : letter2;
    letter2 = surface_time >= 63 && surface_time <= 68 ? 'H' : letter2;
    letter2 = surface_time >= 57 && surface_time <= 62 ? 'I' : letter2;
    letter2 = surface_time >= 51 && surface_time <= 56 ? 'J' : letter2;
    letter2 = surface_time >= 46 && surface_time <= 50 ? 'K' : letter2;
    letter2 = surface_time >= 41 && surface_time <= 45 ? 'L' : letter2;
    letter2 = surface_time >= 37 && surface_time <= 40 ? 'M' : letter2;
    letter2 = surface_time >= 32 && surface_time <= 36 ? 'N' : letter2;
    letter2 = surface_time >= 28 && surface_time <= 31 ? 'O' : letter2;
    letter2 = surface_time >= 24 && surface_time <= 27 ? 'P' : letter2;
    letter2 = surface_time >= 20 && surface_time <= 23 ? 'Q' : letter2;
    letter2 = surface_time >= 16 && surface_time <= 19 ? 'R' : letter2;
    letter2 = surface_time >= 13 && surface_time <= 15 ? 'S' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 12 ? 'T' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 8 ? 'U' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 5 ? 'V' : letter2;
    letter2 = surface_time <= 2 ? 'W' : letter2;
  } else if (letter1 == 'X') {
    letter2 = surface_time >= 174 ? 'A' : letter2;
    letter2 = surface_time >= 126 && surface_time <= 173 ? 'B' : letter2;
    letter2 = surface_time >= 104 && surface_time <= 125 ? 'C' : letter2;
    letter2 = surface_time >= 95 && surface_time <= 103 ? 'D' : letter2;
    letter2 = surface_time >= 87 && surface_time <= 94 ? 'E' : letter2;
    letter2 = surface_time >= 79 && surface_time <= 86 ? 'F' : letter2;
    letter2 = surface_time >= 72 && surface_time <= 78 ? 'G' : letter2;
    letter2 = surface_time >= 66 && surface_time <= 71 ? 'H' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 65 ? 'I' : letter2;
    letter2 = surface_time >= 54 && surface_time <= 59 ? 'J' : letter2;
    letter2 = surface_time >= 49 && surface_time <= 53 ? 'K' : letter2;
    letter2 = surface_time >= 44 && surface_time <= 48 ? 'L' : letter2;
    letter2 = surface_time >= 40 && surface_time <= 43 ? 'M' : letter2;
    letter2 = surface_time >= 35 && surface_time <= 39 ? 'N' : letter2;
    letter2 = surface_time >= 31 && surface_time <= 34 ? 'O' : letter2;
    letter2 = surface_time >= 27 && surface_time <= 30 ? 'P' : letter2;
    letter2 = surface_time >= 23 && surface_time <= 26 ? 'Q' : letter2;
    letter2 = surface_time >= 19 && surface_time <= 22 ? 'R' : letter2;
    letter2 = surface_time >= 16 && surface_time <= 18 ? 'S' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 15 ? 'T' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 11 ? 'T' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 8 ? 'V' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 5 ? 'W' : letter2;
    letter2 = surface_time <= 2 ? 'X' : letter2;
  } else if (letter1 == 'Y') {
    letter2 = surface_time >= 177 ? 'A' : letter2;
    letter2 = surface_time >= 129 && surface_time <= 176 ? 'B' : letter2;
    letter2 = surface_time >= 107 && surface_time <= 128 ? 'C' : letter2;
    letter2 = surface_time >= 98 && surface_time <= 106 ? 'D' : letter2;
    letter2 = surface_time >= 90 && surface_time <= 97 ? 'E' : letter2;
    letter2 = surface_time >= 82 && surface_time <= 89 ? 'F' : letter2;
    letter2 = surface_time >= 75 && surface_time <= 81 ? 'G' : letter2;
    letter2 = surface_time >= 69 && surface_time <= 74 ? 'H' : letter2;
    letter2 = surface_time >= 63 && surface_time <= 68 ? 'I' : letter2;
    letter2 = surface_time >= 57 && surface_time <= 62 ? 'J' : letter2;
    letter2 = surface_time >= 52 && surface_time <= 56 ? 'K' : letter2;
    letter2 = surface_time >= 47 && surface_time <= 51 ? 'L' : letter2;
    letter2 = surface_time >= 42 && surface_time <= 46 ? 'M' : letter2;
    letter2 = surface_time >= 38 && surface_time <= 41 ? 'N' : letter2;
    letter2 = surface_time >= 34 && surface_time <= 37 ? 'O' : letter2;
    letter2 = surface_time >= 30 && surface_time <= 33 ? 'P' : letter2;
    letter2 = surface_time >= 26 && surface_time <= 29 ? 'Q' : letter2;
    letter2 = surface_time >= 22 && surface_time <= 25 ? 'R' : letter2;
    letter2 = surface_time >= 19 && surface_time <= 21 ? 'S' : letter2;
    letter2 = surface_time >= 15 && surface_time <= 18 ? 'T' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 14 ? 'U' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 11 ? 'V' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 8 ? 'W' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 5 ? 'X' : letter2;
    letter2 = surface_time <= 2 ? 'V' : letter2;
  } else if (letter1 == 'Z') {
    letter2 = surface_time >= 180 ? 'A' : letter2;
    letter2 = surface_time >= 132 && surface_time <= 179 ? 'B' : letter2;
    letter2 = surface_time >= 110 && surface_time <= 131 ? 'C' : letter2;
    letter2 = surface_time >= 101 && surface_time <= 109 ? 'D' : letter2;
    letter2 = surface_time >= 92 && surface_time <= 100 ? 'E' : letter2;
    letter2 = surface_time >= 85 && surface_time <= 91 ? 'F' : letter2;
    letter2 = surface_time >= 78 && surface_time <= 84 ? 'G' : letter2;
    letter2 = surface_time >= 72 && surface_time <= 77 ? 'H' : letter2;
    letter2 = surface_time >= 66 && surface_time <= 71 ? 'I' : letter2;
    letter2 = surface_time >= 60 && surface_time <= 65 ? 'J' : letter2;
    letter2 = surface_time >= 55 && surface_time <= 59 ? 'K' : letter2;
    letter2 = surface_time >= 50 && surface_time <= 54 ? 'L' : letter2;
    letter2 = surface_time >= 45 && surface_time <= 49 ? 'M' : letter2;
    letter2 = surface_time >= 41 && surface_time <= 44 ? 'N' : letter2;
    letter2 = surface_time >= 36 && surface_time <= 40 ? 'O' : letter2;
    letter2 = surface_time >= 32 && surface_time <= 35 ? 'P' : letter2;
    letter2 = surface_time >= 29 && surface_time <= 31 ? 'Q' : letter2;
    letter2 = surface_time >= 25 && surface_time <= 28 ? 'R' : letter2;
    letter2 = surface_time >= 21 && surface_time <= 24 ? 'S' : letter2;
    letter2 = surface_time >= 18 && surface_time <= 20 ? 'T' : letter2;
    letter2 = surface_time >= 15 && surface_time <= 17 ? 'U' : letter2;
    letter2 = surface_time >= 12 && surface_time <= 14 ? 'V' : letter2;
    letter2 = surface_time >= 9 && surface_time <= 11 ? 'W' : letter2;
    letter2 = surface_time >= 6 && surface_time <= 8 ? 'X' : letter2;
    letter2 = surface_time >= 3 && surface_time <= 5 ? 'Y' : letter2;
    letter2 = surface_time <= 2 ? 'Z' : letter2;
  }

  return letter2;
}

exports.table1Calculation = table1Calculation;
exports.table2Calculation = table2Calculation;
exports.calculate_rnt_and_abt = calculate_rnt_and_abt;
