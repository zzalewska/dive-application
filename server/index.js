const express = require('express');
const helmet = require('helmet');
const liveReload = require('livereload');
const app = express();
const publicPath = __dirname + '/../web-client/';
const bodyParser = require('body-parser');
let reload;

const { table1Calculation, table2Calculation, calculate_rnt_and_abt } = require('./calculations');
const calculateSac = require('./calculations_sac');

if (process.env.NODE_ENV !== 'production') {
  const liveReloadMiddleware = require('connect-livereload')({
    port: 35729
  });
  reload = liveReload.createServer({
    exts: ['json', 'html', 'css']
  });
  app.use(liveReloadMiddleware);
}

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
      scriptSrc: [
        "'self'",
        'code.jquery.com',
        'maxcdn.bootstrapcdn.com',
        'cdnjs.cloudflare.com',
        'use.fontawesome.com',
        'http://localhost:35729/livereload.js'
      ],
      styleSrc: [
        "'self'",
        'cdn.jsdelivr.net',
        'fonts.googleapis.com',
        'stackpath.bootstrapcdn.com',
        'cdnjs.cloudflare.com',
        'use.fontawesome.com'
      ],
      imgSrc: ["'self'", 'cdnjs.cloudflare.com', 'localhost', 'data:'],
      connectSrc: ["'self'", 'ws://localhost:35729'],
      baseUri: ["'self'"],
      fontSrc: ["'self'", 'fonts.googleapis.com', 'fonts.gstatic.com', 'use.fontawesome.com']
    }
  })
);
app.use(express.static(publicPath));
// app.use('/scripts', express.static(__dirname + '../node_modules/chart.js/dist/'));

app.get('/', (req, res) => res.sendFile('index.html', { root: publicPath }));
app.get('/calculator', (req, res) => res.sendFile('calculator.html', { root: publicPath }));

app.post('/api/letter1', ({ body: { depth1, time } }, res) => {
  const letter = table1Calculation(depth1, time);
  res.json({ letter });
});

app.post('/api/letter2', ({ body: { surface_time, letter1, depth2 } }, res) => {
  const letter2 = table2Calculation(surface_time, letter1);
  const { rnt, abt } = calculate_rnt_and_abt(letter2, depth2);
  res.json({ letter2, rnt, abt });
});

app.post('/api/sac', ({ body: { time_sac, depth_sac, gas, cylinder } }, res) => {
  const sacRE = calculateSac(time_sac, depth_sac, gas, cylinder);
  res.json({sacRE });
});

app.listen(process.env.PORT || 5000, () => console.log(`Server is started`));
if (process.env.NODE_ENV !== 'production') {
  reload.watch(publicPath);
}
