const table1Calculation = require('../calculations');
test('Po 30 minutach na 15 metrach, grupa ciśnieniowa powinna wynosić J', () => {
  expect(table1Calculation(15, 30)).toBe('J');
});
test('Po 40 minutach na 5 metrach, grupa ciśnieniowa powinna wynosić G', () => {
  expect(table1Calculation(5, 40)).toBe('G');
});
test('Po 20 minutach na 25 metrach, grupa ciśnieniowa powinna wynosić K', () => {
  expect(table1Calculation(25, 20)).toBe('K');
});
