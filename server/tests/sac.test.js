const calculateSac = require('../calculations_sac');
test('SAC powinien być 14.4 litrów na minutę.', () => {
  expect(calculateSac(50,15,150,12).toFixed(2)).toBe("14.40");
});

test('SAC powinien być 15.15 litrów na minutę.', () => {
  expect(calculateSac(30,12,100,10).toFixed(2)).toBe("15.15");
});
