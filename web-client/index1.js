window.onload = setCopyrightYear;

function setCopyrightYear() {
    const element = document.getElementById('copyright_full_year');
    if (element) {
        element.innerText = new Date().getFullYear();
    }
}
