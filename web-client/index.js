 window.onload = start;


 let rnt = 0; //residual nitrogen time
 let abt = 0; //actual bottom time
 let tbt = 0; //total bottom time
 //
 function check(e) {
     // Instantiate some variables
     let error = [];

     let rnt = 0; //residual nitrogen time
     let abt = 0; //actual bottom time
     let tbt = 0; //total bottom time
     let surface_time;
     let letter1;
     let letter2;

     const depth1_field = document.getElementById('depth1');
     const time_field = document.getElementById('time');
     const surface_hr_field = document.getElementById('surface_hr');
     const surface_min_field = document.getElementById('surface_min');
     const depth2_field = document.getElementById('depth2');

     const depth1_label = document.getElementById('depth1_label');
     const time_label = document.getElementById('time_label');
     const surface_h_label = document.getElementById('surface_h_label');
     const surface_min_label = document.getElementById('surface_min_label');
     const depth2_label = document.getElementById('depth2_label');
     const output_div = document.getElementById('output');
     const img1_output_div = document.getElementById('img_output1');
     const img2_output_div = document.getElementById('img_output2');
     const grafic_text_small= document.getElementById('grafic_text_small');
     const chart_text = document.getElementById('chart_text');

     depth1_label.className = time_label.className = surface_h_label.className = surface_min_label.className = depth2_label.className = '';

     //Inputs
     const depth1 = parseInt(depth1_field.value);
     const time = parseInt(time_field.value);
     const surface_hr = parseInt(surface_hr_field.value);
     const surface_min = parseInt(surface_min_field.value);
     const depth2 = parseInt(depth2_field.value);

     // Error checks we can do before calculating
     if (Number.isNaN(depth1) || depth1 <= 0 || depth1 > 63) {
         error.push('Głębokość nurkowania powinna być większa niż 0 m i mniejsza niż 42 m!');
         depth1_label.className = 'error';
         depth1_field.focus();
     }

     if (Number.isNaN(time) || time <= 0) {
         error.push('Czas nurkowania powinien być większy od 0 minut!');
         time_label.className = 'error';
         time_field.focus();
     }
     if (Number.isNaN(surface_hr) || Number.isNaN(surface_min)) {
         error.push('Przerwa powierzchniowa powinna trwać od 10 minut do 24 godzin!');
         surface_h_label.className = 'error';
         surface_min_label.className = 'error';
         surface_hr_field.focus();
     } else {
         surface_time = surface_hr * 60 + surface_min;
         if (surface_time < 10 || surface_time > 1440) {
             error.push('Przerwa powierzchniowa powinna trwać od 10 minut do 24 godzin!');
             surface_h_label.className = 'error';
              surface_min_label.className = 'error';
             surface_hr_field.focus();
         }
     }

     if (Number.isNaN(depth2) || depth2 <= 0 || depth2 > 63) {
         error.push('Głębokość powtórnego nurkowania powinna być większa niż 0 m i mniejsza niż 42 m!');
         depth2_label.className = 'error';
         depth2_field.focus();
     }



     if (error.length) {
         output_errors(error, output_div);
         return false;
     }

     fetch('/api/letter1', {
             method: 'post',
             body: new URLSearchParams({
                 depth1,
                 time
             })
         })
         .then((response) => response.json())
         .then(({
             letter
         }) => {
             letter1 = letter;

             // Errors check
             if (letter1 === '') {
                 error.push(
                     'Czas/głębokość przekracza<br />limit z tablic bezdekompresyjnych.<br /><br />Wprowadź płytszą głębokość lub krótszy czas.'
                 );
                 time_label.className = 'error';
                 time_field.focus();
                 output_errors(error, output_div);
                 return false;
             }
             fetch('/api/letter2', {
                     method: 'post',
                     body: new URLSearchParams({
                         surface_time,
                         letter1,
                         depth2
                     })
                 })
                 .then((response) => response.json())
                 .then((data) => {
                     letter2 = data.letter2;
                     rnt = data.rnt;
                     abt = data.abt;
                     printResult(letter1, letter2, abt, rnt, depth1, depth2, output_div, img1_output_div, img2_output_div,grafic_text_small,chart_text);
                     chartDiagram(depth1, depth2, time, abt, surface_time);
                 });
         });

     return false;
 }

 function printResult(letter1, letter2, abt, rnt, depth1, depth2, output_div, img1_output_div, img2_output_div, grafic_text_small,chart_text) {
     let printed_answer =
         'Grupa ciśnieniowa po pierwszym nurkowaniu: <b>' +
         letter1 +
         '</b> .<br />Grupa ciśnieniowa po przerwie powierzchniowej: <b>' +
         letter2 + "</b>";

     if (abt && rnt) {
         printed_answer +=
             '<br />Czas resztkowego azotu: <b>' +
             rnt +
             ' minut</b>.<br />Rzeczywisty dozwolony czas podczas drugiego nurkowania na ' +
             depth2 +
             ' m. to <b>' +
             abt +
             ' minut</b>.';
     } else if (rnt) {
         printed_answer =
             printed_answer +
             '<br />Czas resztkowego azotu: ' +
             rnt +
             ' minut.<br />Kolejne bezdekompresyjne nurkowanie jest niemożliwe. ';
     }

     if ((depth1 >= 25) || (depth1 > 22 && time > 23) ||
       (depth1 >= 20 && time > 30) || (depth1 >= 18 && time > 38) ||
       (depth1 >= 16 && time > 48) || (depth1 >= 14 && time > 60) ||
       (depth1 >= 12 && time > 77) || (depth1 >= 10 && time > 77) || (depth1 < 10 && time > 145)) {
       printed_answer += "<br /><br />UWAGA! <i>Wymagany przystanek bezpieczeństwa po pierwszym nurkowaniu</i>.";
   }

   if ((letter1 == "W") || (letter1 == "X")) {
       printed_answer += "<br /><br />UWAGA! <i>Wymagana przerwa powierzchniowa trwająca min. 1 godzinę (Zasada WXYX)</i>.";
   }

   if ((letter1 == "Y") || (letter1 == "Z")) {
       printed_answer += "<br /><br />UWAGA! <i>Wymagana przerwa powierzchniowa trwająca min. 3 godzin (Zasada WXYX)</i>.";
   }


     output_div.innerHTML = printed_answer;
    // arrow_div.innerHTML = '➡';

    if(letter1!=letter2)
    {
    grafic_text_small.innerHTML = "Grupa ciśnieniowa po pierwszym nurkowaniu to: <b>" + letter1 + "</b><br>Po przerwie ciśnieniowej zredukowała się do: <b>" +
    letter2 + "</b><br><br> Tak się zmienia nasycenie azotu w Twoim organiźmie";
  }else{
    grafic_text_small.innerHTML = "Grupa ciśnieniowa po pierwszym nurkowaniu to: <b>" + letter1 + "</b><br>Po przerwie ciśnieniowej wynosi nadal: <b>" +
    letter2 + "</b><br><br> Tak się zmienia nasycenie azotu w Twoim organiźmie";
  }

     let img_answer = imgGenerator(letter1);
     img1_output_div.innerHTML = img_answer;

     img_answer = imgGenerator(letter2);
     img2_output_div.innerHTML = img_answer;

     if(depth2>depth1)
     {
          chart_text.innerHTML = "Drugie nurkowanie jest głębsze niż pierwsze,<br><b id='chart_alert_error'> nurkowanie niezgodne z prawidłowym profilem nurkowania!</b><br><br>" +
          "Zawsze staraj się najgłebsze nurkowania wykonywać na początku dnia ";
          chart_text.className = "error";
     }else{
       chart_text.innerHTML = "Drugie nurkowanie nie jest głębsze od pierwszego,<br><br><b id='chart_alert_correct'> nurkowanie jest zgodne z prawidłowym profilem nurkowania!</b><br><br>";
     }


     grafic_text_small.className = output_div.className = img2_output_div.className = img1_output_div.className = 'result';

 }

 /**
  * Displays errors from an array
  */
 function output_errors(errors, output_div) {
     errors = errors.join('<br />');
     output_div.innerHTML = errors;
     output_div.className = 'error';
 }

 /**
  * Removes error messages
  */
 function remove_errors() {
     const output_div = document.getElementById('output');
     const img1_output_div = document.getElementById('img_output1');
     const img2_output_div = document.getElementById('img_output2');
     const chart_text = document.getElementById('chart_text');
    chart_text.innerHTML = output_div.innerHTML =  img2_output_div.innerHTML = '';
    grafic_text_small.innerHTML = "Przed pierwszym nurkowaniem poziom zaadsorbowanego azotu w naszym organizmie nie ma wpływu na nurkowanie";
    img1_output_div.innerHTML = "<img class='img_sizer' src='/images/nitrogen_images/0.png'>";
    chartDiagram(0,0,0,0,0);

     const depth1_label = document.getElementById('depth1_label');
     const time_label = document.getElementById('time_label');
     const surface_h_label = document.getElementById('surface_h_label');
     const surface_min_label = document.getElementById('surface_min_label');
     const depth2_label = document.getElementById('depth2_label');

     chart_text.className = output_div.className = depth1_label.className = time_label.className = surface_h_label.className = surface_min_label.className = depth2_label.className = '';
 }

 function sac_remove_errors() {
     const sac_output_div = document.getElementById('sac_output');
     sac_output_div.innerHTML = '';

     const time_sac_label = document.getElementById('time_sac_label');
     const depth_sac_label = document.getElementById('depth_sac_label');
     const gas_label = document.getElementById('gas_label');
     const cylinder_label = document.getElementById('cylinder_label');

     time_sac_label.className = depth_sac_label.className = gas_label.className = cylinder_label.className = '';
 }

 function sac_check(e) {

     let sac = 0;
     let error = [];

     const time_sac_field = document.getElementById('time_sac');
     const depth_sac_field = document.getElementById('depth_sac');
     const gas_field = document.getElementById('gas');
     const cylinder_field = document.getElementById('cylinder');


     const time_sac_label = document.getElementById('time_sac_label');
     const depth_sac_label = document.getElementById('depth_sac_label');
     const gas_label = document.getElementById('gas_label');
     const cylinder_label = document.getElementById('cylinder_label');
     const sac_output_div = document.getElementById('sac_output');
     time_sac_label.className = depth_sac_label.className = gas_label.className = cylinder_label.className = '';
     //
     // //Inputs
     const time_sac = parseInt(time_sac_field.value);
     const depth_sac = parseInt(depth_sac_field.value);
     const gas = parseInt(gas_field.value);
     const cylinder = parseInt(cylinder_field.value);

     // Error checks we can do before calculating
     if (Number.isNaN(time_sac) || time_sac <= 0) {
         error.push('łębokość nurkowania powinna być większa niż 0 m');
         time_sac_label.className = 'error';
         time_sac_field.focus();
     }

     if (Number.isNaN(depth_sac) || depth_sac <= 0) {
         error.push('Czas nurkowania powinien być większy od 0 minut!');
         depth_sac_label.className = 'error';
         depth_sac_field.focus();
     }
     if (Number.isNaN(gas) || gas <= 0) {
         error.push('Ilość zużytego gazu powinien byćwiększy od 0 barów!');
         gas_label.className = 'error';
         gas_field.focus();
     }
     if (Number.isNaN(cylinder) || cylinder <= 0) {
         error.push('Pojemność butli powinna być większa od 0 litrów!');
         depth2_label.className = 'error';
         depth2_field.focus();
     }

     if (error.length) {
         output_errors(error, output_div);
         return false;
     }

     fetch('/api/sac', {
             method: 'post',
             body: new URLSearchParams({
                 time_sac,
                 depth_sac,
                 gas,
                 cylinder
             })
         })
         .then((response) => response.json())
         .then(({
             sacRE
         }) => {
             sac = sacRE;

             sac_output_div.innerHTML = "Podczas <b> " + time_sac +
                 " minutowego</b> nurkowania na <b> " + depth_sac +
                 " metrach</b>, gdzie spadek ciśnienia w butli wynosił <b>" + gas + " barów</b>, przy <b>" +
                 cylinder + " litrowej</b> butli,<b> zużycie powietrza wynosi " + sac.toFixed(2) + " litrów na minutę.</b>";

             sac_output_div.className = 'result';

         });

     return false;
 }

 /**
  * Loading is done
  */
 function start() {
     sacForm();
     initForm();


     chartDiagram(0,0,0,0,0);
     setCopyrightYear();
 }

 function initForm() {
     const form = document.getElementById('diveform');
     if (form) {
         form.onsubmit = check;
         const resetButton1 = document.getElementById('remove_errors');

         if (resetButton1) {
             resetButton1.onclick = remove_errors;
         }
     }
     setDefaultFocus('depth1');

 }

 function sacForm() {
     const form = document.getElementById('sacform');
     const output = document.getElementById('sac_output');
     if (form) {
         form.onsubmit = sac_check;
         const resetButton2 = document.getElementById('remove_errors2');

         if (resetButton2) {
             resetButton2.onclick = sac_remove_errors;
         }
     }
     setDefaultFocus('time_sac');

 }

 function setDefaultFocus(name) {
     const element = document.getElementById(name);
     if (element) {
         element.focus();
     }
 }

 function setCopyrightYear() {
     const element = document.getElementById('copyright_full_year');
     if (element) {
         element.innerText = new Date().getFullYear();
     }
 }

 function imgGenerator(letter) {
     let img_answer = "";

     img_answer = "<img class='img_sizer'src='/images/nitrogen_images/" + letter + ".png'>";
     return img_answer;
 }

 function chartDiagram(depth1, depth2, time1, time2, surf_time) {
     let first_dive_start = depth1 / 10;
     let first_dive_end = time1 - depth1 / 10;
     let surface_time = time1 + surf_time;
     let second_dive_start = surface_time + depth2 / 10;
     let end_time = surface_time + time2;
     let second_dive_end = end_time - depth2 / 10;

     new Chart(document.getElementById("myChart"), {
         type: 'line',
         data: {
             labels: [0, first_dive_start, first_dive_end, time1, surface_time, second_dive_start, second_dive_end, end_time],
             datasets: [{
                 data: [0, depth1, depth1, 0, 0, depth2, depth2, 0, 0],
                 borderColor: "#0b2239",
                 fill: false,
                 lineTension: 0
             }]
         },
         options: {
           responsive: true,
           maintainAspectRatio: false,
             scales: {
                 yAxes: [{
                     ticks: {
                         max: 42,
                         min: 0,
                         stepSize: 10,
                         reverse: true
                     },
                     scaleLabel: {
                         display: true,
                         labelString: 'Głębokość [m]'
                     }

                 }],
                 xAxes: [{
                     display: true,
                     scaleLabel: {
                         display: true,
                         labelString: 'Czas [min]'
                     }
                 }]
             },
             legend: {
                 display: false
             },
             animation: {
                 duration: 2500,
                 easing: 'linear'
             },
             title: {
               display: true,
               position: 'top',
               text: "Wykres przedstawiający profil nurkowania",
               fontSize: 20,
               fontColor: "#0b2239",
               lineHeight: 4
             }

         }
     });
 }
